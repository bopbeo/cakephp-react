class Index extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			products: []
		}
	}

	componentDidMount() {
		axios.get('http://localhost/products.json')

		.then(res => {
	        const products = res.data;
	        this.setState({ products });
      	})
      	.catch(error => console.log(error));
	}

	handleDelete(product_id) 
	{
		if (confirm("Are You Delete Now ?"))
		{
			axios.delete('http://localhost/products/' + product_id + '.json')
			const products = this.state.products.filter(products => products.id !== product_id);
			this.setState({ products });
		}
	}

	render() {
		return (
			<div>
				<br />
				<a href="/products/add" className="btn btn-primary">Create New Record</a>
				<br /><br />
				<div className="row">
					<table className="table table-hover">
						<thead>
							<tr className="table-active">
								<th>Name</th>
								<th>Description</th>
								<th style={{width: '230px'}}>Action</th>
							</tr>
						</thead>
						<tbody>
							{this.state.products.map((product, index) => (
								<tr key={index}>
									<td>{product.name}</td>
									<td>{product.description}</td>
									<td>
										<a href={'/products/view/' + product.id} className="btn btn-outline-info">View</a>
										<a href={'/products/edit/' + product.id} className="btn btn-outline-primary">Edit</a>
									 	<button type="submit" className=" delete btn btn-outline-danger" onClick = {this.handleDelete.bind(this, product.id)}>Delete</button>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<Index />,
	document.getElementById('index')
);

// $(document).on('click', '.delete', function(event){
// 	event.preventDefault();
// 	var id = $(this).attr('id');

// 	axios.delete(`http://localhost/products/viewOne/` + id + `.json`)
// 		.then(res => {
// 	        console.log(res);
// 	        console.log(res.data);
// 	        const product = this.state.products.filter(p => p.id !== id);
// 	        this.setState({ product });
//       	})
// });

// handleDelete(product_id) 
// 	{
// 		console.log(product_id);
// 		axios.delete(`http://localhost/products/delete/` + product_id)
// 		.then(res => {
// 	        var products = this.this.state.products;

// 	        for (var i = 0; i < products.length; i++) {
// 	        	if (products[i].id == product_id) 
// 	        	{
// 	        		products.splice(i,1);
// 	        		this.setState({products:products});
// 	        	}
// 	        }
//       	});     
// 	}

