class Edit extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div>
				<fieldset>
				<legend>Edit Product</legend>
				<div className="form-group">
					<div className="col-lg-10">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" placeholder="Name" required="required" defaultValue={name} />
					</div>
				</div>

				<div className="form-group">
					<div className="col-lg-10">
						<label for="name">Description</label>
						<textarea name="description" class="form-control" placeholder="Description" required="required" rows="5" defaultValue={description} ></textarea> 
					</div>
				</div>

				<button class="btn btn-primary" type="submit">Update</button>
				<a href="/products" class="btn btn-primary">Back</a>
				</fieldset>
			</div>
		);
	}
}

var element = document.getElementById('edit');

if(element)
{
	var name = element.dataset.name;
	var description = element.dataset.description;

	ReactDOM.render(
		<Edit name = {name} description = {description} />,
		element
	);
}

