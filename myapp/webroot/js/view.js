class View extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div>
		        <div className="card text-info mb-3" style={{marginTop: '50px'}}>
		          <div className="card-header bg-info text-white">InFo&nbsp; {name}</div>
		          <table className="table-bordered">
		            <tbody><tr>
		                <th scope="row">ID</th>
		                <td>{id}</td>
		              </tr>
		              <tr>
		                <th scope="row">Name</th>
		                <td>{name}</td>
		              </tr>
		              <tr>
		                <th scope="row">Description</th>
		                <td>{description}</td>
		              </tr>
		              <tr>
		                <th scope="row">Created</th>
		                <td>{created}</td>
		              </tr>
		              <tr>
		                <th scope="row">Modified</th>
		                <td>{modified}</td>
		              </tr>
		            </tbody></table>
		        </div>
		        <div>
		          <a href="/products" className="btn btn-primary">Back</a>
		        </div>
      		</div>
		);
	}
}

var element = document.getElementById('view');
if(element)
{
	var id = element.dataset.id;
	var name = element.dataset.name;
	var description = element.dataset.description;
	var created = element.dataset.created;
	var modified = element.dataset.modified;

	ReactDOM.render(
		<View id = {id} name = {name} description = {description} created = {created} modified = {modified} />,
		element
	);
}


