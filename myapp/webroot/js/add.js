class Add extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div>
				<fieldset>
				<legend>ADD Product</legend>
				<div className="form-group">
					<div className="col-lg-10">
						<div className="input text required">
							<label for="name">Name</label>
							<input type="text" name="name" className="form-control" placeholder="Name" required="required" maxlength="255" id="name" />
						</div>
					</div>
				</div>

				<div className="form-group">
					<div className="col-lg-10">
						<label for="name">Description</label>
						<textarea name="description" className="form-control" placeholder="Description" required="required" rows="5"></textarea> 
					</div>
				</div>

				<button className="btn btn-primary" type="submit">Add</button>
				<a href="/products" className="btn btn-primary">Back</a>
				</fieldset>
			</div>
		);
	}
}

ReactDOM.render(
	<Add />,
	document.getElementById('create')
);
