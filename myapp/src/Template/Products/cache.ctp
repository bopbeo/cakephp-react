<br />
<?php echo $this->Html->link('Create New Record', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
<br /><br />
<div class="row">
    <table class="table table-hover">
        <thead>
            <tr class="table-active">
                <th>Name</th>
                <th>Description</th>
                <th style="width: 220px;">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
            <?php if(!empty($products)):?>
            <?php foreach($products as $product):?>
            <tr>
                <td><?php echo $product->name;?></td>
                <td><?php echo $product->description;?></td>
                <td>
                    <?php echo $this->Html->link('View', ['action' => 'view', $product->id], ['class' => 'btn btn-outline-info']);?>
                    <?php echo $this->Html->link('Edit', ['action' => 'edit', $product->id], ['class' => 'btn btn-outline-primary']);?>
                    <?= $this->Form->postLink('Delete', ['action' => 'delete', $product->id], ['class' => 'btn btn-outline-danger', 'confirm' => 'Are you sure you want to delete # {0}?', $product->id]);?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php else:?>
            <td>No Record Found!</td>
        <?php endif;?>
        </tbody>
    </table>
</div>


<?php echo $this->Form->create($product);?>
<fieldset>
  <legend>ADD Product</legend>

  <div class="form-group">
    <div class="col-lg-10">
      <?php echo $this->Form->input('name', ['class' => 'form-control', 'Placeholder' => 'Name']);?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-10">
      <?php echo $this->Form->textarea('description', ['class' => 'form-control', 'Placeholder' => 'Description']);?>
    </div>
  </div>
  
  <?php echo $this->Form->button('Add', ['class' => 'btn btn-primary']);?>
  <?php echo $this->Html->link('Back', ['action' => 'index'], ['class' => 'btn btn-primary']);?>
</fieldset>
<?php echo $this->Form->end();?>




<?php echo $this->Form->create($product);?>
<fieldset>
  <legend>Update Product</legend>

  <div class="form-group">
    <div class="col-lg-10">
      <?php echo $this->Form->control('name', ['class' => 'form-control', 'Placeholder' => 'Name']);?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-10">
      <?php echo $this->Form->textarea('description', ['class' => 'form-control', 'Placeholder' => 'Description']);?>
    </div>
  </div>
  
  <?php echo $this->Form->button('Edit', ['class' => 'btn btn-primary']);?>
  <?php echo $this->Html->link('Back', ['action' => 'index'], ['class' => 'btn btn-primary']);?>
</fieldset>
<?php echo $this->Form->end();?>






<div class="card text-info mb-3" style="margin-top: 50px;">
    <div class="card-header bg-info text-white">InFo&nbsp<?= h($product->name) ?></div>
    <table class="table-bordered">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($product->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($product->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($product->modified) ?></td>
        </tr>
    </table>
</div>
<div>
    <?php echo $this->Html->link('Back', ['action' => 'index'], ['class' => 'btn btn-primary']);?>
</div>
