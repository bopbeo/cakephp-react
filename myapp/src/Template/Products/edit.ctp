<?php echo $this->Form->create($product);?>

<div id="edit" data-name="<?php echo $product->name?>" data-description="<?php echo $product->description?>"></div>
<?php echo $this->Form->end();?>

<?= $this->Html->script('edit', ['type' => 'text/babel']) ?>
