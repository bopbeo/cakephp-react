<?php
$cakeDescription = 'CRUD cakephp';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div id="header"></div>

    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>

    <footer>
    </footer>
</body>
</html>
<?= $this->Html->script('jquery-3.3.1') ?>
<?= $this->Html->script('bootstrap') ?>
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<?= $this->Html->script('https://unpkg.com/@babel/standalone/babel.min.js') ?>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<?= $this->Html->script('header', ['type' => 'text/babel']) ?>







