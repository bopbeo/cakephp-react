<?php
namespace App\Controller;

use App\Controller\AppController;

class ProductsController extends AppController
{
   
    public function index()
    {
        //$this->set('products', $this->paginate($this->Products->find('all')));
        $products = $this->Products->find('all');
        $this->set(['products'=>$products,'_serialize'=>'products']);
    }

   
    public function view($id = null)
    {
        // $product = $this->Products->get($id);
        // $this->set('product', $product);
        
        $product = $this->Products->get($id);

        $this->set(['product'=>$product,'_serialize'=>'product']);

    }

    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success('The product has been saved');

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('The product could not be saved. Please, try again');
        }
        $this->set(compact('product'));
    }

    public function edit($id = null)
    {
        $product = $this->Products->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $this->set(compact('product'));
    }

    public function delete($id = null)
    {

        //$this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            // return $this->response->type(['success' => 'The product has been deleted']);
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            // return $this->response->type(['error' => 'he product could not be deleted. Please, try again']);
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        // return $this->redirect(['action' => 'index']);

    }
}
